import glob
import sys
from os import stat
import json

print('Number of arguments:', len(sys.argv), 'arguments.')

print('Argument List:', str(sys.argv))

all_videos=[]

dups={}

number=0

def err(message=''):
    sys.stderr.write(str(message)+'\n')
    sys.stderr.flush() #err

def same(base_vid, compared_vid):
    print('same('+str(base_vid)+','+str(compared_vid)+')?')
    if stat(base_vid).st_size!=stat(compared_vid).st_size:
        return False
    with open(base_vid,'rb') as filea, open(compared_vid,'rb') as fileb:
        return filea.read()==fileb.read() #same

for arg in sys.argv:
    for video_one in glob.iglob(arg*,
                                recursive=True):
        all_videos.append(video_one)

print(all_videos)



for video in all_videos:
    number+=1
    duplicate=False
    for key in dups:
        err(number)
        if same(key,video):
            dups[key]+=[video]
            duplicate=True
            break # for key in dups
    if not duplicate:
        dups[video]=[]

        
with open('vids_and_dups.json', 'w') as v_and_d:
    for vids,dups in dups.items():
        v_and_d.write('%s:%s\n' % (vids, dups))


