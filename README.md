Find Duplicate Files.

The reason for this script was to help me find duplicates of videos which I had stored on an external drive.

## Installation
Download the python file to your home system!

## Usage
{name of module}.py <folder_path>
Will find all duplicate videos in folder_path, generating a report.

## Support
Questions can be sent by posting a message at www.legocoder.com/contact/